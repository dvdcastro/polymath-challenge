# Requests for getting HTTP stuff
import requests

# Parsing XML
from lxml import etree

# Getting command line stuff
import sys, getopt

# Sqlite data handling
import sqlite3

# Template engine
import jinja2

ebay_common_headers = {
    'X-EBAY-API-APP-NAME': 'EchoBay62-5538-466c-b43b-662768d6841',
    'X-EBAY-API-CERT-NAME': '00dd08ab-2082-4e3c-9518-5f4298f296db',
    'X-EBAY-API-DEV-NAME': '16a26b1b-26cf-442d-906d-597b60c41c19',
    'X-EBAY-API-SITEID': '0',
    'X-EBAY-API-COMPATIBILITY-LEVEL': '861',
}

ebay_categories_header = {
    'X-EBAY-API-CALL-NAME': 'GetCategories'
}

ebay_categories_data = """
<?xml version="1.0" encoding="utf-8"?>
<GetCategoriesRequest xmlns="urn:ebay:apis:eBLBaseComponents">
  <CategorySiteID>0</CategorySiteID>
  <DetailLevel>ReturnAll</DetailLevel>
  <RequesterCredentials>
    <eBayAuthToken>AgAAAA**AQAAAA**aAAAAA**PMIhVg**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GhCpaCpQWdj6x9nY+seQ**L0MCAA**AAMAAA**IahulXaONmBwi/Pzhx0hMqjHhVAz9/qrFLIkfGH5wFH8Fjwj8+H5FN4NvzHaDPFf0qQtPMFUaOXHpJ8M7c2OFDJ7LBK2+JVlTi5gh0r+g4I0wpNYLtXnq0zgeS8N6KPl8SQiGLr05e9TgLRdxpxkFVS/VTVxejPkXVMs/LCN/Jr1BXrOUmVkT/4Euuo6slGyjaUtoqYMQnmBcRsK4xLiBBDtiow6YHReCJ0u8oxBeVZo3S2jABoDDO9DHLt7cS73vPQyIbdm2nP4w4BvtFsFVuaq6uMJAbFBP4F/v/U5JBZUPMElLrkXLMlkQFAB3aPvqZvpGw7S8SgL7d2s0GxnhVSbh4QAqQrQA0guK7OSqNoV+vl+N0mO24Aw8whOFxQXapTSRcy8wI8IZJynn6vaMpBl5cOuwPgdLMnnE+JvmFtQFrxa+k/9PRoVFm+13iGoue4bMY67Zcbcx65PXDXktoM3V+sSzSGhg5M+R6MXhxlN3xYfwq8vhBQfRlbIq+SU2FhicEmTRHrpaMCk4Gtn8CKNGpEr1GiNlVtbfjQn0LXPp7aYGgh0A/b8ayE1LUMKne02JBQgancNgMGjByCIemi8Dd1oU1NkgICFDbHapDhATTzgKpulY02BToW7kkrt3y6BoESruIGxTjzSVnSAbGk1vfYsQRwjtF6BNbr5Goi52M510DizujC+s+lSpK4P0+RF9AwtrUpVVu2PP8taB6FEpe39h8RWTM+aRDnDny/v7wA/GkkvfGhiioCN0z48</eBayAuthToken>
  </RequesterCredentials>
</GetCategoriesRequest>
"""


# Category parsing dictionary
def assignCategoryID(cat, text):
    cat['CategoryID'] = text
def assignCategoryName(cat, text):
    cat['CategoryName'] = text
def assignCategoryLevel(cat, text):
    cat['CategoryLevel'] = text
def assignBestOfferEnabled(cat, text):
    cat['BestOfferEnabled'] = text

cat_parse_dict = {
    '{urn:ebay:apis:eBLBaseComponents}CategoryID': assignCategoryID,
    '{urn:ebay:apis:eBLBaseComponents}CategoryName': assignCategoryName,
    '{urn:ebay:apis:eBLBaseComponents}CategoryLevel': assignCategoryLevel,
    '{urn:ebay:apis:eBLBaseComponents}BestOfferEnabled': assignBestOfferEnabled,
}

ebay_api_url = 'https://api.sandbox.ebay.com/ws/api.dll'

def get_ebay_categories():
    request_headers = dict(ebay_categories_header);
    request_headers.update(ebay_common_headers)
    try:
        r = requests.post(ebay_api_url, headers=request_headers, data=ebay_categories_data)
        return r.text
    except requests.exceptions.Timeout:
        # Maybe set up for a retry, or continue in a retry loop
        print('[ERROR] Connection timeout, please try again later.')
        sys.exit(1)
    except requests.exceptions.TooManyRedirects:
        # Tell the user their URL was bad and try a different one
        print('[ERROR] Redirects error, please try again later.')
        sys.exit(1)
    except requests.exceptions.RequestException as e:
        # catastrophic error. bail.
        print('[ERROR] Connection error, please try again later.')
        sys.exit(1)

def rebuild_category_db(no_web):
    conn = sqlite3.connect('ebay_categories.sqlite3')

    # Persist eBay data in a file first
    if not no_web:
        print('[INFO] Getting data from eBay')
        root = etree.fromstring(get_ebay_categories().encode('utf-8'))

        print('[INFO] Printing data info categories.xml')
        f = open('categories.xml', 'wb')
        f.write(etree.tostring(root, pretty_print=True))
        f.close()
    else:
        print('[INFO] Using cached data info categories.xml')

    # Parse file
    parser = etree.XMLParser(ns_clean=True)
    tree = etree.parse('categories.xml', parser)
    root = tree.getroot()
    cats = []
    for category in root.iter('{urn:ebay:apis:eBLBaseComponents}Category'):
        cat = {}
        for attr in category:
            if attr.tag in cat_parse_dict:
                cat_parse_dict[attr.tag](cat, attr.text)
        cats.append(cat)

    # print(*cats, sep='\n')

    # Load into sqlite3
    c = conn.cursor()
    # Create table
    c.execute('''
        CREATE TABLE IF NOT EXISTS ebay_category (
            CategoryID INTEGER PRIMARY KEY ASC,
            CategoryName TEXT,
            CategoryLevel INTEGER,
            BestOfferEnabled INTEGER
        );
    ''')
    # Clear table
    c.execute('''
        DELETE FROM ebay_category;
    ''')

    # Insert query
    batch_size = 500 # Limit of compund selects in sqlite
    query_start = '''
INSERT INTO ebay_category
'''
    first = True
    count = 0
    insert_query = '';

    insert_query += query_start
    for cat in cats:
        cat_query_str = 'SELECT '
        cat_query_str += cat['CategoryID'] + (" AS 'CategoryID', " if first else ', ')
        cat_query_str += "'" + cat['CategoryName'].replace("'","''") + "'" + (" AS 'CategoryName', " if first else ', ')
        cat_query_str += cat['CategoryLevel'] + (" AS 'CategoryLevel', " if first else ', ')
        if 'BestOfferEnabled' in cat:
            cat_query_str += ('1' if cat['BestOfferEnabled'] == 'true' else '0') + (" AS 'BestOfferEnabled' " if first else ' ') + "\n"
        else:
            cat_query_str += "0 " + ("AS 'BestOfferEnabled' " if first else ' ') + "\n"

        first = False
        count += 1

        cat_query_str += 'UNION ALL '

        insert_query += cat_query_str

        if count >= batch_size:
            insert_query = insert_query[:-11]
            insert_query += ';'

            # print(insert_query)

            c.execute(insert_query)

            insert_query = ''
            insert_query += query_start
            first = True
            count = 0


    insert_query = insert_query[:-11]
    insert_query += ';'

    # print(insert_query)

    c.execute(insert_query)
    conn.commit()
    conn.close()

    print('[INFO] Database loaded')

def render_category(categoryId):
    conn = sqlite3.connect('ebay_categories.sqlite3')
    c = conn.cursor()

    templateLoader = jinja2.FileSystemLoader( searchpath="./" )
    templateEnv = jinja2.Environment( loader=templateLoader )

    template_file = "cat_template.jinja"
    template = templateEnv.get_template( template_file )

    print('[INFO] Rendering Category ID =', categoryId)
    t = (categoryId,)
    c.execute('SELECT * FROM ebay_category WHERE CategoryID = ?', t)

    category = {}
    row = c.fetchone()

    if row is None:
        print('[USER_ERROR] No category with ID:',categoryId)
        return

    category['CategoryID'] = row[0]
    category['CategoryName'] = row[1]
    category['CategoryLevel'] = row[2]
    category['BestOfferEnabled'] = 'Yes' if row[3] == 1 else 'No'

    conn.close()

    text_file = open(categoryId + ".html", "w")
    text_file.write(template.render(category))
    text_file.close()

    print('[INFO] HTML file for Category ID =', categoryId, 'rendered.')
    print('%s.html' % categoryId)

def main(argv):
    categoryId = 'none'
    rebuild = False
    no_web = False

    #print('Number of arguments:', len(sys.argv), 'arguments.')
    #print('Argument List:', str(sys.argv))

    usage = 'Usage: python categories.py (--rebuild) (--render <category_id>)'

    try:
        opts, args = getopt.getopt(argv,"hsr:n",["rebuild","render=","no-web"])
    except getopt.GetoptError:
        print(usage)
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print(usage)
            sys.exit()
        elif opt in ("-s", "--rebuild"):
            rebuild = True
        elif opt in ("-r", "--render"):
            categoryId = arg
        elif opt in ("-n", "--no-web"):
            no_web = True

    if not(rebuild) and categoryId == 'none':
        print(usage)
    else:
        if rebuild :
            print('[INFO] Rebuilding categories database')
            rebuild_category_db(no_web)

        if categoryId != 'none' :
            render_category(categoryId)

if __name__ == "__main__":
   main(sys.argv[1:])
