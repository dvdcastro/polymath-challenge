# Polymath challenge

## Setup

1. [Install python3 for mac and enable a virtual env to start using this project](http://www.marinamele.com/2014/07/install-python3-on-mac-os-x-and-use-virtualenv-and-virtualenvwrapper.html)
2. Install requests ```pip install requests```
3. Install lxml ```pip install lxml```
4. Install Jinja2 ```pip install Jinja2```

## Usage

```bash
# Rebuild database
python3 categories.py --rebuild

# Render category with id 179281
python3 categories.py --render 179281
```
